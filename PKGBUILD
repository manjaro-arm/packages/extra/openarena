# Contributor: Martijn Braam <martijn@brixit.nl>
# Maintainer: Furkan Salman <fk@fkardame.com>

pkgname=openarena
pkgver=0.8.8
pkgrel=1
pkgdesc="A violent, sexy, multiplayer first person shooter based on the ioquake3 engine (binary package)"
arch=('aarch64')
url="http://openarena.ws/"
license=('GPL2')
depends=('sdl' 'libvorbis' 'curl')
makedepends=('mesa' 'glu')
optdepends=('openal')
_commit="f507a7a87a86878d4d3b72315b9d9fe8c255e1e3"
_gc_commit="b7e01f40c433a602987be2a95fb13385e163a3c4"
source=("$pkgname-$_commit.tar.gz::https://github.com/OpenArena/engine/archive/$_commit.tar.gz"
	"$pkgname-$_gc_commit.tar.gz::https://github.com/OpenArena/gamecode/archive/$_gc_commit.tar.gz"
	"$pkgname-data-$pkgver.zip::http://openarena.ws/request.php?4"
	'engine-aarch64.patch'
	'gamecode-aarch64.patch'
	'openarena.desktop'
	'openarena.png')
sha512sums=('f835fc3ff872d356f57628e2475e1f52d4a9594e897801c6eb3e6ab70fc62439014fdf84c0a0e98597a7e29cb6f765bc6aaaf10bdb4acb2bc60c10e27a12bba2'
            '3a50e099cc5a812ed88089c801793a9ae6cf7a6a200da016623bed4980a694d10d2870d472cbfeb36ca5426bf21b2078092791e1001206f6bd5c1a03de068704'
            '9fa4dabe8a3428dc3cbec97f3b4d20c04569c14cdd00b60e6391c6dd61e310f246ff5ec97e7549821b3d6f5f94b140eb5411a2ddd83dafcad66937b7f78ea8dd'
            '2c074da36161509501d4fd5c8bd66d6c2e04803a12eff22ee235531314d355b97214bf83d3bb6095607467c66074f2366ac100af745e86ad7b457bec4dc5fa62'
            'ae48c7f61b59f4fa34accaadbddff9189bb112b014b9efdfe9c692a788bf9ca173d9ec2940d318a5c728c54ff5aeaf976470f7029ce2a903283f5cdbbb87fa5c'
            'c4dcb3fcbedeabbce7ed85f0964f4a1d20aab5da1d57e1dbf2ee99ca6b42e083bc82e178930712fd8afebaacac729ab83c1aa5ce05a8375ab8ab3b5314c74e74'
            'ef68f8eb6251c3424464702ff894a6b88b473a3f4c1512af613125f5e5a7124f268490a9f6042095ff5bb807817e1f302c80d21987a2ed178e680f993d70b6f1')

builddir="engine-$_commit"
builddir_gc="gamecode-$_gc_commit"
builddir_data="openarena-$pkgver"

prepare() {
	cd "${srcdir}/$builddir"
	patch -p1 -i "$srcdir"/engine-aarch64.patch
	cd "${srcdir}/$builddir_gc"
	patch -p1 -i "$srcdir"/gamecode-aarch64.patch
}

build() {
  cd "${srcdir}/openarena-${pkgver}"
  rm *.dll *.exe
  rm -rf __MACOSX *.app
  
  # Build engine
  cd "${srcdir}/$builddir"
  make USE_CODEC_XMP=0

  # Build gamecode
  cd "${srcdir}/$builddir_gc"
  make 
}

package() {
	_qarch=${CARCH}
	case "$_qarch" in
	armv7) _qarch="armv7l" ;;
	esac

	cd "${srcdir}/$builddir"
	mkdir -p "$pkgdir"/usr/share/games/openarena
	
	echo "Add gamedata"
	cp -rv "${srcdir}/$builddir_data"/baseoa "$pkgdir"/usr/share/games/openarena/
	
	echo "Add engine"
	cp -rv "${srcdir}/$builddir"/build/release-linux-$_qarch/* "$pkgdir"/usr/share/games/openarena/

	echo "Add gamecode"
	cp -rv "${srcdir}/$builddir_gc"/build/release-linux-$_qarch/* "$pkgdir"/usr/share/games/openarena/

    chmod 755 "$pkgdir"/usr/share/games/openarena/baseoa

    chmod 755 "$pkgdir"/usr/share/games/openarena/openarena.aarch64
    #install -dm755 "${pkgdir}/usr/bin/"
    install -Dm644 "${srcdir}/openarena.png" -t "${pkgdir}/usr/share/pixmaps/"
    install -Dm644 "${srcdir}/openarena.desktop" -t "${pkgdir}/usr/share/applications/"
    #ln -s /usr/share/games/openarena/openarena.aarch64 "$pkgdir"/usr/bin/openarena
}


#AUR
#package() {
 # output="${pkgdir}/opt"
  #install -dm755 "${output}" "$pkgdir"/usr/share/pixmaps/ "$pkgdir"/usr/share/applications/ "$pkgdir"/usr/bin
  #mv "${srcdir}/openarena-${pkgver}" "${pkgdir}/opt/openarena"
  #find "${pkgdir}/opt/openarena" -type f -exec chmod 644 {} \;
  #find "${pkgdir}/opt/openarena" -type d -exec chmod 755 {} \;
  #chmod 755 "${pkgdir}"/opt/openarena/*.{i386,x86_64}
#install -Dm 644 "$srcdir"/openarena{,-server}.png "$pkgdir"/usr/share/pixmaps/
 # install -Dm 644 "$srcdir"/openarena{,-server}.desktop "$pkgdir"/usr/share/applications/
#  install -Dm 755 "$srcdir"/openarena-runner.sh "$pkgdir"/opt/openarena/openarena-runner.sh
 # ln -s /opt/openarena/openarena-runner.sh "$pkgdir"/usr/bin/openarena
 #ln -s /opt/openarena/openarena-runner.sh "$pkgdir"/usr/bin/openarena-server
#}
